const { default: LoginPage } = require("../PageObect/LoginPage")

describe('CostumSuite', function () {
    before(function() {
        cy.fixture('example').then(function(data){

            this.data=data
        })
            
        })
        it('login into the website', function()  
        {
            const login = new LoginPage()
            login.visit()
            login.fillCredentials(this.data.username,this.data.password)
            login.submit()
            login.verification()
        })
      })
    