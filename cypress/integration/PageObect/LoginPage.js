class LoginPage
{

visit()
{
    cy.visit('https://katalon-demo-cura.herokuapp.com/profile.php#login')
}

fillCredentials(username, password)
{
    const userNameField = cy.get('#txt-username')
    userNameField.clear()
    userNameField.type(username)

    const passwordField = cy.get('#txt-password')
    passwordField.clear()
    passwordField.type(password)

    return this
}

submit()
{
    cy.get('#btn-login').click()
}

verification()
{
    cy.title().should('eq', 'CURA Healthcare Service')
}

}

export default LoginPage